module.exports = (app) => {
  app.use('/api/suppliers', require('./api/supplier'));
  app.use('/api/belanja', require('./api/belanja'));
  app.use('/api/customers', require('./api/customer'));
  app.use('/api/returs', require('./api/retur'));
  app.use('/api/purchase-orders', require('./api/PurchaseOrder'));
  app.use('/api/barang', require('./api/barang'));
  app.use('/api/stocks', require('./api/stock'));
  app.use('/api/penjualan', require('./api/penjualan'));
  app.use('/api/waitings', require('./api/waiting'));
  app.use('/api/dashboards', require('./api/dashboard'));
  app.use('/api/users', require('./api/user'));

  app.use('/auth/local', require('./api/auth'));
  // app.route('/*')
  //   .get((req, res) => {
  //     res.sendfile(app.get('appPath') + '/index.html');
  //   });
};