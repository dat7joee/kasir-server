const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');
const passport = require('passport');
const port = 8000;

mongoose.connect(config.db, {
	socketTimeoutMS: 30000,
	keepAlive: true,
	reconnectTries: 30000
});
mongoose.connection.on('connected', () => {
  console.log('Database Connected: Kasir');
});
mongoose.connection.on('error', (error) => {
  console.log('Failed to Connect Database!', error);
});

const app = express();

app.use(cors());

app.use(express.static(path.join(__dirname, 'dist')));

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));

require('./routes')(app);

app.use(passport.initialize());
app.use(passport.session());
require('./api/auth/passport')(passport);

app.listen(port, () => console.log(`Server running on PORT ${port}`));