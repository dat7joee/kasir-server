const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const SupplierSchema = new Schema({
  code: { type: String, required: true, trim: true, unique: true },
  name: { type: String, required: true, trim: true },
  address: { type: String, trim: true },
  phone: { type: String },
  transaction: {
    pembelian: { type: Number, default: 0 },
    hutang: { type: Number, default: 0 }
  },
  hutangLogs: [
    {
      total: { type: Number },
      created: { type: Date, default: function () { return new Date(); } }
    }
  ],
  created: { type: Date, default: function () { return new Date(); } }
});

SupplierSchema.pre('save', function (next) {
  var tgl = new Date();
  var dt = new Date(tgl.getFullYear(), tgl.getMonth(), tgl.getDate());
  console.log(this);
  if (this.isNew) {
    var self = this;
    mongoose.model('Supplier', SupplierSchema).count({ created: { $gte: dt } }, function (err, count) {
      var nextCode = "" + (count + 1);
      var code = 'SU' + getRandomID() + getOrderDate() + '0000';
      var codeSupplier = code.substr(0, code.length - nextCode.length) + nextCode;
      self.code = codeSupplier;
      next();
    });
  } else {
    next();
  }
});

SupplierSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Supplier', SupplierSchema);

function getOrderDate() {
  var tanggal = new Date;
  var dd = tanggal.getDate();
  var mm = tanggal.getMonth() + 1;
  if (dd < 10) {
    dd = '0' + dd;
  }
  if (mm < 10) {
    mm = '0' + mm;
  }

  return tanggal.getFullYear().toString().substr(-2) + mm + dd;
}

function getRandomID() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  for (var i = 0; i < 3; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}