const _ = require('lodash');
const Supplier = require('./supplier.model');

exports.index = (req, res) => {
  Supplier.paginate({code: {$regex: req.query.code, $options: "i"}, name: {$regex: req.query.name, $options: "i"}}, {page: Number(req.query.page), limit: Number(req.query.limit)}, function(err, suppliers){
    console.log('err', err);
    if(err) return res.send(500, err);
    return res.json(200, suppliers);
  });
};

exports.show = function(req, res) {
  Supplier.findById(req.params.id, function(err, supplier){
    if(err) return res.send(500, err);
    if(!supplier) return res.send(404, {message: "Supplier Not Found!"});
    return res.json(200, supplier);
  });
};

exports.search = (req, res) => {
  Supplier.find({name: {$regex: req.query.name, $options: "i"}}).exec(function(err, suppliers){
    if(err) return res.send(500, err);
    return res.json(200, suppliers);
  });
};

exports.create = (req, res) => {
  var body = req.body;
  body.code = "SU";
  Supplier.create(body, function (err, supplier) {
    if (err) return handleError(res, err);
    return res.json(201, supplier);
  });
};

exports.update = (req, res) => {
  if (req.body._id) { delete req.body._id };
  Supplier.findById(req.params.id, function (err, supplier) {
    if (err) return handleError(res, err);
    if (!supplier) return res.send(404, { message: 'Supplier Not Found!' });
    let updated = _.merge(supplier, req.body);
    updated.save(function (err) {
      if (err) return handleError(res, err);
      return res.json(200, supplier);
    });
  });
};

exports.destroy = (req, res) => {
  Supplier.findById(req.params.id, function (err, supplier) {
    if (err) return handleError(res, err);
    if (!supplier) return res.send(404, { message: 'Supplier not Found' });
    supplier.remove(function (err) {
      if (err) return handleError(res, err);
      return res.send(204);
    })
  });
};

function handleError(res, err) {
  return res.send(500, err);
}