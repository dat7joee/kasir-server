const express = require('express');
const router = express.Router();
const controller = require('./barang.controller');
const passport = require('passport');

router.get('/', passport.authenticate('jwt', {session: false}), controller.index);
router.get('/search', passport.authenticate('jwt', {session: false}), controller.search);
router.get('/find', passport.authenticate('jwt', {session: false}), controller.find);
router.get('/export', passport.authenticate('jwt', {session: false}), controller.excel);
router.get('/:id', passport.authenticate('jwt', {session: false}), controller.show);
router.post('/', passport.authenticate('jwt', {session: false}), controller.create);
router.post('/upload', passport.authenticate('jwt', {session: false}), controller.upload);
router.put('/:id', passport.authenticate('jwt', {session: false}), controller.update);
router.delete('/:id', passport.authenticate('jwt', {session: false}), controller.destroy);

module.exports = router;