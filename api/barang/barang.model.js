const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const BarangSchema = new Schema({
  code: { type: String, required: true, trim: true },
  name: { type: String, required: true, trim: true },
  satuan: { type: String, trim: true },
  jenis_harga: { type: String, trim: true },
  primaryPrice: { type: Number, default: 0 },
  sellingPrice: { type: Number, default: 0 },
  stock: { type: Schema.Types.ObjectId, ref: 'Stock' },
  created: { type: Date, default: function () { return new Date(); } }
});

BarangSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Barang', BarangSchema);

function getOrderDate() {
  var tanggal = new Date;
  var dd = tanggal.getDate();
  var mm = tanggal.getMonth() + 1;
  if (dd < 10) {
    dd = '0' + dd;
  }
  if (mm < 10) {
    mm = '0' + mm;
  }

  return tanggal.getFullYear().toString().substr(-2) + mm + dd;
}

function getRandomID() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  for (var i = 0; i < 3; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}