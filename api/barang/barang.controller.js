const _ = require('lodash');
const Barang = require('./barang.model');
const Stock = require('../stock/stock.model');
const Q = require('q');

exports.index = (req, res) => {
  var query = {
    code: { $regex: req.query.code, $options: "i" },
    name: { $regex: req.query.name, $options: "i" },
    satuan: { $regex: req.query.satuan, $options: "i" },
    jenis_harga: { $regex: req.query.jenis_harga, $options: "i" }
  };
  Barang.paginate(query, { populate: 'stock', page: Number(req.query.page), limit: Number(req.query.limit) }, function (err, barang) {
    if (err) return res.send(500, err);
    return res.json(200, barang);
  });
};

exports.search = (req, res) => {
  Barang.find({ name: { $regex: req.query.name, $options: "i" } }).populate('stock').exec(function (err, barang) {
    if (err) return res.send(500, err);
    return res.json(200, barang);
  });
};

exports.find = (req, res) => {
  Barang.findOne({ code: req.query.code }).populate('stock').exec(function (err, barang) {
    if (err) return res.send(500, err);
    if (!barang) return res.send(404);
    return res.json(200, barang);
  });
};

exports.show = (req, res) => {
  Barang.findOne({ _id: req.params.id }).populate('stock').exec(function (err, barang) {
    if (err) return res.send(500, err);
    if (!barang) return res.send(404);
    return res.json(200, barang);
  });
};

exports.excel = (req, res) => {
  Barang.find().populate({ path: 'stock', select: 'in out stock' }).sort({ code: 1 }).exec(function(err, barangs){
    if(err) return res.send(500, err);
    var lists = [];
    for(var i = 0; i < barangs.length;i++) {
      lists.push({
        code: barangs[i].code,
        name: barangs[i].name,
        satuan: barangs[i].satuan,
        jenis_harga: barangs[i].jenis_harga,
        primaryPrice: barangs[i].primaryPrice,
        sellingPrice: barangs[i].sellingPrice,
        in_stock: barangs[i].stock.in,
        out_stock: barangs[i].stock.out,
        current_stock: barangs[i].stock.stock
      });
    }

    res.json(200, lists);
  });
};

exports.create = (req, res) => {
  var body = req.body;
  body.code = body.code.toUpperCase();
  // var stok = checkSatuan(body.stock, body.satuan);
  var newStock = {
    stock: body.stock,
    in: body.stock,
    out: 0
  };
  Stock.create(newStock, function (err, stock) {
    if (err) return res.send(500, err);
    body.stock = stock._id;
    Barang.create(body, function (err, barang) {
      if (err) return handleError(res, err);
      return res.json(201, barang);
    });
  });
};

function checkSatuan(qty, satuan) {
  var newQty = 0;
  switch (satuan) {
    case "lsn":
      newQty = qty * 12;
      break;

    case "kodi":
      newQty = qty * 20;
      break;

    case "gross":
      newQty = qty * 144;
      break;

    default:
      newQty = qty;
  }
  return newQty;
}


exports.upload = (req, res) => {
  let body = req.body;
  var promises = [];
  body.map(function (brg) {
    let newStock = {
      stock: brg.stock,
      in: brg.stock,
      out: 0
    };
    promises.push(createStock(newStock, brg));
  });
  Q.all(promises).then(function (stocks) {
    // var promisesBrg = [];
    // stocks.map(function(brg){
    //   body.stock = brg._id;
    //   promisesBrg.push(createBarang())
    // });
    console.log(stocks);
    var created = stocks.filter(function (stock) {
      return stocks.status === 'created';
    });
    var exist = stocks.filter(function (stock) {
      return stock.status === 'exist';
    });
    return res.json(200, { created: created.length, exist: exist.length });
  });
};

function createStock(stock, barang) {
  return Barang.findOne({ code: barang.code }).exec().then(function (hasbarang) {
    if (hasbarang) return { status: "exist", barang: hasbarang };
    return Stock.create(stock).then(function (data) {
      barang.stock = data._id;
      return Barang.create(barang).then(function (brg) {
        return { status: "created", barang: brg };
      }).then(null, function (err) {
        if (err) console.log(err);
      });
    }).then(null, function (err) {
      if (err) console.log(err);
    });
  }).then(null, function (err) {
    if (err) console.log(err);
  });
}

exports.update = (req, res) => {
  if (req.body._id) { delete req.body._id };
  Barang.findById(req.params.id, function (err, barang) {
    if (err) return handleError(res, err);
    if (!barang) return res.send(404, { message: 'Barang Not Found!' });
    var stock = barang.stock;
    let updated = _.merge(barang, req.body);
    updated.stock = stock;
    updated.save(function (err) {
      if (err) return handleError(res, err);
      return res.json(200, barang);
    });
  });
};

exports.destroy = (req, res) => {
  Barang.findById(req.params.id, function (err, barang) {
    if (err) return handleError(res, err);
    if (!barang) return res.send(404, { message: 'Barang not Found' });
    Stock.findOne({ _id: barang.stock }).exec(function (err, result) {
      if (err) return handleError(res, err);
      if (!result) return res.send(404);
      result.remove(function (err) {
        if (err) return handleError(res, err);
        barang.remove(function (err) {
          if (err) return handleError(res, err);
          return res.send(204);
        });
      });
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}