const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const BelanjaSchema = new Schema({
  code: { type: String, required: true, trim: true, unique: true },
  description: { type: String, required: true, trim: true },
  qty: { type: Number, default: 0 },
  price: { type: Number, default: 0 },
  total: { type: Number, default: 0 },
  created: { type: Date, default: function () { return new Date(); } }
});

BelanjaSchema.pre('save', function (next) {
  var tgl = new Date();
  var dt = new Date(tgl.getFullYear(), tgl.getMonth(), tgl.getDate());
  console.log(this);
  if (this.isNew) {
    var self = this;
    mongoose.model('Belanja', BelanjaSchema).count({ created: { $gte: dt } }, function (err, count) {
      var nextCode = "" + (count + 1);
      var code = 'BL' + getRandomID() + getOrderDate() + '0000';
      var codeBelanja = code.substr(0, code.length - nextCode.length) + nextCode;
      self.code = codeBelanja;
      next();
    });
  } else {
    next();
  }
});

BelanjaSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Belanja', BelanjaSchema);

function getOrderDate() {
  var tanggal = new Date;
  var dd = tanggal.getDate();
  var mm = tanggal.getMonth() + 1;
  if (dd < 10) {
    dd = '0' + dd;
  }
  if (mm < 10) {
    mm = '0' + mm;
  }

  return tanggal.getFullYear().toString().substr(-2) + mm + dd;
}

function getRandomID() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  for (var i = 0; i < 3; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}