const express = require('express');
const router = express.Router();
const controller = require('./belanja.controller');
const passport = require('passport');

router.get('/', passport.authenticate('jwt', {session: false}), controller.index);
router.get('/:id', passport.authenticate('jwt', {session: false}), controller.show);
router.post('/', passport.authenticate('jwt', {session: false}), controller.create);
router.put('/:id', passport.authenticate('jwt', {session: false}), controller.update);
router.delete('/:id', passport.authenticate('jwt', {session: false}), controller.destroy);

module.exports = router;