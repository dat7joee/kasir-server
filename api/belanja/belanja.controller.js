const _ = require('lodash');
const Belanja = require('./belanja.model');

exports.index = (req, res) => {
  Belanja.paginate({ code: { $regex: req.query.code, $options: "i" } }, { page: Number(req.query.page), limit: Number(req.query.limit) }, function (err, belanja) {
    if (err) return res.send(500, err);
    return res.json(200, belanja);
  });
};

exports.show = (req, res) => {
  Belanja.findOne({ _id: req.params.id }).exec(function (err, belanja) {
    if (err) return res.send(500, err);
    if (!belanja) return res.send(404, { message: "Belanja Not Found!" });
    return res.json(200, belanja);
  });
};


exports.create = (req, res) => {
  var body = req.body;
  body.code = "BJ";
  body.total = body.qty * body.price;
  Belanja.create(body, function (err, belanja) {
    if (err) return handleError(res, err);
    return res.json(201, belanja);
  });
};

exports.update = (req, res) => {
  if (req.body._id) { delete req.body._id };
  Belanja.findById(req.params.id, function (err, belanja) {
    if (err) return handleError(res, err);
    if (!belanja) return res.send(404, { message: 'belanja Not Found!' });
    let updated = _.merge(belanja, req.body);
    updated.save(function (err) {
      if (err) return handleError(res, err);
      return res.json(200, belanja);
    });
  });
};

exports.destroy = (req, res) => {
  Belanja.findById(req.params.id, function (err, belanja) {
    if (err) return handleError(res, err);
    if (!belanja) return res.send(404, { message: 'belanja not Found' });
    belanja.remove(function (err) {
      if (err) return handleError(res, err);
      return res.send(204);
    })
  });
};

function handleError(res, err) {
  return res.send(500, err);
}