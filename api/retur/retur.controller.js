const _ = require('lodash');
const Retur = require('./retur.model');

exports.index = (req, res) => {
  Retur.paginate({ code: { $regex: req.query.code, $options: "i" } }, { populate: 'customer detail.barang', page: Number(req.query.page), limit: Number(req.query.limit) }, function (err, returs) {
    if (err) return res.send(500, err);
    return res.json(200, returs);
  });
};

exports.show = (req, res) => {
  Retur.findOne({ _id: req.params.id }).populate('customer detail.barang').exec(function (err, retur) {
    if (err) return res.send(500, err);
    if (!retur) return res.send(404, { message: "Retur Not Found!" });
    console.log(retur);
    return res.json(200, retur);
  });
};

exports.create = (req, res) => {
  var body = req.body;
  body.code = "RT";
  body.customer = body.customer ? body.customer._id : null;
  var detail = [];
  body.items.map(function(item){
    detail.push({
      barang: item._id,
      qty: item.qty,
      price: item.price,
      subtotal: item.qty * item.price
    });
  });
  body.detail = detail;
  delete body.items;
  console.log(body);
  // return res.json(200);
  Retur.create(body, function (err, retur) {
    if (err) return handleError(res, err);
    return res.json(201, retur);
  });
};

exports.update = (req, res) => {
  if (req.body._id) { delete req.body._id };
  Retur.findById(req.params.id, function (err, retur) {
    if (err) return handleError(res, err);
    if (!retur) return res.send(404, { message: 'Retur Not Found!' });
    let updated = _.merge(retur, req.body);
    updated.save(function (err) {
      if (err) return handleError(res, err);
      return res.json(200, retur);
    });
  });
};

exports.destroy = (req, res) => {
  Retur.findById(req.params.id, function (err, retur) {
    if (err) return handleError(res, err);
    if (!retur) return res.send(404, { message: 'Retur not Found' });
    retur.remove(function (err) {
      if (err) return handleError(res, err);
      return res.send(204);
    })
  });
};

function handleError(res, err) {
  return res.send(500, err);
}