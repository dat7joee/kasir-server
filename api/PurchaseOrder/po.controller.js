const _ = require('lodash');
const PurchaseOrder = require('./po.model');
const Supplier = require('../supplier/supplier.model');
const Stock = require('../stock/stock.model');
const Barang = require('../barang/barang.model');
const Q = require('q');

exports.index = (req, res) => {
  PurchaseOrder.paginate({ code: { $regex: req.query.code, $options: "i" }, supplierName: {$regex: req.query.supplierName, $options: "i"} }, { sort: {created: -1}, populate: 'supplier detail.barang', page: Number(req.query.page), limit: Number(req.query.limit) }, function (err, pos) {
    if (err) return res.send(500, err);
    return res.json(200, pos);
  });
};

exports.show = (req, res) => {
  PurchaseOrder.findOne({ _id: req.params.id }).populate('supplier detail.barang').exec(function (err, po) {
    if (err) return res.send(500, err);
    if (!po) return res.send(404, { message: "PurchaseOrder Not Found!" });
    return res.json(200, po);
  });
};

exports.report = (req, res) => {
  var tgl = new Date();
  var day = tgl.getDate();
  var month = tgl.getMonth();
  var year = tgl.getFullYear();
  var ad = new Date(year, month, day, 00, 00, 01);
  var sd = new Date(year, month, day, 23, 59, 59);
  PurchaseOrder.paginate({supplierName: {$regex: req.query.supplierName, $options: 'i'}, created: {$gte: ad, $lte: sd}}, {populate: 'customer detail.barang', page: Number(req.query.page), limit: Number(req.query.limit)}, function(err, penjualan){
    if(err) return res.send(500, err);
    return res.json(200, penjualan);
  });
};

exports.create = (req, res) => {
  var body = req.body;
  var promises = [];
  var total = 0;
  body.detail.map(function(dt){
    // dt.qty = checkSatuan(dt.qty, dt.satuan);
    dt.subtotal = dt.qty * dt.price;
    total += dt.subtotal;
    dt.barang = dt._id;
    delete dt.code;
    delete dt.name;
  });
  body.total = total;
  body.supplierName = body.supplier ? body.supplier.name : null;
  body.supplier = body.supplier ? body.supplier._id : null;
  console.log(body);
  body.code = "PO";
  // return res.json(200);
  Supplier.findOne({_id: body.supplier}).exec(function(err, result){
    if(err) return res.send(500, err);
    if(!result) return res.send(404, {message: 'Supplier Tidak Terdaftar!'});
    result.transaction.pembelian += body.total;
    if(body.uang) {
      result.transaction.hutang = body.uang < body.total ? result.transaction.hutang + (body.total - body.uang) : result.transaction.hutang;
      if(body.uang < body.total) {
        result.hutangLogs.push({
          total: (body.total - body.uang)
        });
      }
    }
    result.markModified('hutangLogs');
    result.save(function(err){
      if(err) return res.send(500, err);
      PurchaseOrder.create(body, function (err, po) {
        if (err) return handleError(res, err);
        body.detail.map(function(dtl){
          promises.push(changeStock(dtl));
        });
        Q.all(promises).then(function(data){
          console.log(data);
          return res.json(201, po);
        });
      });
    });
  });
};

function changeStock(barang) {
  return Barang.findOne({_id: barang.barang}).exec().then(function(brg){
    if(!brg) return {status: 'not_exist', barang: brg};
    return Stock.findOne({_id: brg.stock}).exec().then(function(stok){
      if(!stok) return {status: "not_exist"};
      stok.stock += barang.qty;
      // stok.primaryPrice = barang.price;
      stok.in += barang.qty;
      return stok.save().then(function(data){
        return {status: "updated_stok", barang: barang};
      });
    }).then(null, function(err){
      if(err) console.log(err);
    });
  }).then(null, function(err){
    if(err) console.log(err);
  });
}

function checkSatuan(qty, satuan) {
  var newQty = 0;
  switch(satuan) {
    case "lsn":
    newQty = qty * 12;
    break;

    case "kodi":
    newQty = qty * 20;
    break;

    case "gross":
    newQty = qty * 144;
    break;

    default:
    newQty = qty;
  }
  return newQty;
}

exports.update = (req, res) => {
  if (req.body._id) { delete req.body._id };
  PurchaseOrder.findById(req.params.id, function (err, po) {
    if (err) return handleError(res, err);
    if (!po) return res.send(404, { message: 'Purchase Order Not Found!' });
    let updated = _.merge(po, req.body);
    updated.save(function (err) {
      if (err) return handleError(res, err);
      return res.json(200, po);
    });
  });
};

exports.destroy = (req, res) => {
  PurchaseOrder.findById(req.params.id, function (err, po) {
    if (err) return handleError(res, err);
    if (!po) return res.send(404, { message: 'Purchase Order not Found' });
    po.remove(function (err) {
      if (err) return handleError(res, err);
      return res.send(204);
    })
  });
};

function handleError(res, err) {
  return res.send(500, err);
}