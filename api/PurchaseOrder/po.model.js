const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const PurchaseOrderSchema = new Schema({
  code: { type: String, required: true, trim: true, unique: true },
  supplier: { type: Schema.Types.ObjectId, required: true, ref: 'Supplier' },
  supplierName: { type: String, trim: true },
  total: { type: Number, default: 0 },
  disc: { type: Number, default: 0 },
  tempo: { type: Date },
  detail: [
    {
      barang: { type: Schema.Types.ObjectId, required: true, ref: 'Barang' },
      qty: { type: Number, default: 0 },
      satuan: { type: String },
      price: { type: Number, default: 0 },
      subtotal: { type: Number, default: 0 }
    }
  ],
  created: { type: Date, default: function () { return new Date(); } }
});

PurchaseOrderSchema.pre('save', function (next) {
  var tgl = new Date();
  var dt = new Date(tgl.getFullYear(), tgl.getMonth(), tgl.getDate());
  console.log(this);
  if (this.isNew) {
    var self = this;
    mongoose.model('PurchaseOrder', PurchaseOrderSchema).count({ created: { $gte: dt } }, function (err, count) {
      var nextCode = "" + (count + 1);
      var code = 'PO' + getRandomID() + getOrderDate() + '0000';
      var codePurchaseOrder = code.substr(0, code.length - nextCode.length) + nextCode;
      self.code = codePurchaseOrder;
      next();
    });
  } else {
    next();
  }
});

PurchaseOrderSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('PurchaseOrder', PurchaseOrderSchema);

function getOrderDate() {
  var tanggal = new Date;
  var dd = tanggal.getDate();
  var mm = tanggal.getMonth() + 1;
  if (dd < 10) {
    dd = '0' + dd;
  }
  if (mm < 10) {
    mm = '0' + mm;
  }

  return tanggal.getFullYear().toString().substr(-2) + mm + dd;
}

function getRandomID() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  for (var i = 0; i < 3; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}