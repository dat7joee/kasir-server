const _ = require('lodash');
const Stock = require('./stock.model');

exports.index = (req, res) => {
  Stock.paginate({}, { populate: 'barang', page: Number(req.query.page), limit: Number(req.query.limit) }, function (err, barang) {
    if (err) return res.send(500, err);
    return res.json(200, barang);
  });
};

exports.show = (req, res) => {
  Stock.findOne({ _id: req.params.id }).populate('barang').exec(function (err, barang) {
    if (err) return res.send(500, err);
    if (!barang) return res.send(404, { message: "Stock Not Found!" });
    return res.json(200, barang);
  });
};

exports.create = (req, res) => {
  var body = req.body;
  Stock.create(body, function (err, stock) {
    if (err) return handleError(res, err);
    return res.json(201, stock);
  });
};

exports.update = (req, res) => {
  if (req.body._id) { delete req.body._id };
  Stock.findById(req.params.id, function (err, stock) {
    if (err) return handleError(res, err);
    if (!stock) return res.send(404, { message: 'Stock Barang Not Found!' });
    let updated = _.merge(stock, req.body);
    updated.save(function (err) {
      if (err) return handleError(res, err);
      return res.json(200, stock);
    });
  });
};

exports.destroy = (req, res) => {
  Stock.findById(req.params.id, function (err, stock) {
    if (err) return handleError(res, err);
    if (!stock) return res.send(404, { message: 'Stock Barang not Found' });
    stock.remove(function (err) {
      if (err) return handleError(res, err);
      return res.send(204);
    })
  });
};

function handleError(res, err) {
  return res.send(500, err);
}