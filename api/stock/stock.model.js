const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const StockSchema = new Schema({
  // barang: { type: Schema.Types.ObjectId, required: true, ref: 'Barang' },
  in: { type: Number, default: 0 },
  out: { type: Number, default: 0 },
  stock: { type: Number, default: 0 },
  created: { type: Date, default: function () { return new Date(); } }
});

StockSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Stock', StockSchema);