const _ = require('lodash');
const User = require('./user.model');
const bcrypt = require('bcryptjs');

exports.index = (req, res) => {
  User.paginate({}, {page: Number(req.query.page), limit: Number(req.query.limit)} ,function (err, users) {
    if (err) return handleError(res, err);
    return res.json(200, users);
  });
};

exports.me = (req, res) => {
  User.findById(req.params.id, '-password', function (err, user) {
    if (err) return handleError(res, err);
    if (!user) return res.send(404);
    return res.json(200, user);
  });
};

exports.create = (req, res) => {
  let newUser = new User(req.body);
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(newUser.password, salt, (err, hash) => {
      if (err) throw err;
      newUser.password = hash;
      newUser.save(function (err) {
        if (err) return handleError(res, err);
        return res.json(201, newUser);
      });
    });
  });
};

exports.update = (req, res) => {
  if (req.body._id) { delete req.body._id };
  User.findById(req.params.id, function (err, user) {
    if (err) return handleError(res, err);
    if (!user) return res.send(404, { message: 'User Not Found!' });
    let updated = _.merge(user, req.body);
    updated.save(function (err) {
      if (err) return handleError(res, err);
      return res.json(200, user);
    });
  });
};

exports.destroy = (req, res) => {
  User.findById(req.params.id, function (err, user) {
    if (err) return handleError(res, err);
    if (!user) return res.send(404, { message: 'User not Found' });
    user.remove(function (err) {
      if (err) return handleError(res, err);
      return res.send(204);
    })
  });
};

function handleError(res, err) {
  return res.send(500, err);
}