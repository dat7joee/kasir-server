const express = require('express');
const router = express.Router();
const controller = require('./user.controller');
const passport = require('passport');
const User = require('./user.model');
const bcrypt = require('bcryptjs');

router.get('/', controller.index);
router.get('/seed-root', (req, res) => {
   User.count({role: 'root'}, function(err, result){
      if(err) return res.send(500, err);
      if(result > 0) {
         return res.json(200, {message: "user Exist!"});
      } else {
         let newUser = new User({
            username: 'polaris',
            password: 'linuxer7',
            role: 'root',
            name: 'Software Creator'
         });
         bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            newUser.save(function (err) {
              if (err) return handleError(res, err);
              return res.json(201, newUser);
            });
          });
        });
      }
   });
});
router.get('/:id', passport.authenticate('jwt', { session: false }), controller.me);
router.post('/', passport.authenticate('jwt', {session: false}), controller.create);
router.put('/:id', passport.authenticate('jwt', {session: false}), controller.update);
router.delete('/:id', passport.authenticate('jwt', {session: false}), controller.destroy);

module.exports = router;