const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const UserSchema = new Schema({
  username: { type: String, required: true, trim: true, unique: true },
  password: { type: String, required: true },
  name: { type: String, required: true, trim: true },
  role: { type: String, required: true },
  created: { type: Date, default: function () { return new Date(); } }
});

UserSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('User', UserSchema);