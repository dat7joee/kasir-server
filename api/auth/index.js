const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('../../config');
const User = require('../user/user.model');

router.post('/', (req, res, next) => {
  User.findOne({ username: req.body.username }, function (err, user) {
    const userData = user;
    if (err) return res.send(500, err);
    if (!user) return res.json(404, { message: 'User Not Found!' });
    comparePassword(req.body.password, user.password, function (err, isMatch) {
      if (err) return res.send(500, err);
      if (isMatch) {
        const token = jwt.sign({
          _id: userData._id,
          username: userData.username,
          name: userData.name,
          role: userData.role,
          created: userData.created
        }, config.secret, {
            expiresIn: 604800
          });
        return res.json(200, { access_token: 'JWT ' + token, user: user });
      } else {
        return res.send(401, { message: 'Username and Password not Match , Invalid Login!' });
      }
    });
  });
});

module.exports = router;

function comparePassword(candidate, hash, cb) {
  bcrypt.compare(candidate, hash, (err, isMatch) => {
    if (err) throw err;
    cb(null, isMatch);
  });
}