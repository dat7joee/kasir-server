const express = require('express');
const router = express.Router();
// const controller = require('./supplier.controller');
const Retur = require('../retur/retur.model');
const Penjualan = require('../penjualan/penjualan.model');
const Pembelian = require('../PurchaseOrder/po.model');
const Waiting = require('../waiting/waiting.model');
const Q = require('q');
const passport = require('passport');

router.get('/', passport.authenticate('jwt', {session: false}), (req, res) => {
  var tgl = new Date();
  var day = tgl.getDate();
  var month = tgl.getMonth();
  var year = tgl.getFullYear();
  var ad = new Date(year, month, day, 00, 00, 01);
  var sd = new Date(year, month, day, 23, 59, 59);
  var promises = [getReturs(ad, sd), getWaiting(ad, sd), getPenjualan(ad, sd), getPembelian(ad, sd)];
  Q.all(promises).then(function (data) {
    var waiting = data.filter(function (wait) {
      return wait.type === 'waiting'
    });
    var retur = data.filter(function (rt) {
      return rt.type === 'retur'
    });
    var penjualan = data.filter(function (pj) {
      return pj.type === 'penjualan'
    });
    var pembelian = data.filter(function (pb) {
      return pb.type === 'pembelian'
    });
    return res.json(200, { waiting: waiting[0].data, retur: retur[0].data, penjualan: penjualan[0].data, pembelian: pembelian[0].data });
  });
});

function getWaiting(ad, sd) {
  return Waiting.aggregate([
    { $match: { created: { $gte: ad, $lte: sd } } },
    { $group: { _id: "grandTotal", total: { $sum: "$grandTotal" }, count: { $sum: 1 } } }
  ]).exec().then(function (data) {
    return { type: 'waiting', data: data[0] };
  }).then(null, function (err) {
    if (err) console.log(err);
  });
}

function getPenjualan(ad, sd) {
  return Penjualan.aggregate([
    { $match: { created: { $gte: ad, $lte: sd } } },
    { $group: { _id: "grandTotal", total: { $sum: "$grandTotal" }, count: { $sum: 1 } } }
  ]).exec().then(function (data) {
    return { type: 'penjualan', data: data[0] };
  }).then(null, function (err) {
    if (err) console.log(err);
  });
}

function getPembelian(ad, sd) {
  return Pembelian.aggregate([
    { $match: { created: { $gte: ad, $lte: sd } } },
    { $group: { _id: "total", total: { $sum: "$total" }, count: { $sum: 1 } } }
  ]).exec().then(function (data) {
    return { type: 'pembelian', data: data[0] };
  }).then(null, function (err) {
    if (err) console.log(err);
  });
}

function getReturs(ad, sd) {
  return Retur.aggregate([
    { $match: { created: { $gte: ad, $lte: sd } } },
    { $group: { _id: "total", total: { $sum: "$total" }, count: { $sum: 1 } } }
  ]).exec().then(function (data) {
    return { type: 'retur', data: data[0] };
  }).then(null, function (err) {
    if (err) console.log(err);
  });
}

module.exports = router;