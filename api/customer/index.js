const express = require('express');
const router = express.Router();
const controller = require('./customer.controller');
const passport = require('passport');

router.get('/', passport.authenticate('jwt', {session: false}), controller.index);
router.get('/search', passport.authenticate('jwt', {session: false}), controller.search);
router.get('/export', passport.authenticate('jwt', {session: false}), controller.excel); 
router.get('/backup', passport.authenticate('jwt', {session: false}), controller.backupTransaction); 
router.get('/:id', passport.authenticate('jwt', {session: false}), controller.show);
router.post('/', passport.authenticate('jwt', {session: false}), controller.create);
router.put('/:id', passport.authenticate('jwt', {session: false}), controller.update);
router.delete('/:id', passport.authenticate('jwt', {session: false}), controller.destroy);

module.exports = router;