const _ = require('lodash');
const Q = require('q');
const Customer = require('./customer.model');

exports.index = (req, res) => {
  let query = { code: { $regex: req.query.code, $options: "i" }, name: { $regex: req.query.name, $options: "i" } };
  Customer.paginate(query, { page: Number(req.query.page), limit: Number(req.query.limit) }, function (err, customers) {
    if (err) return res.send(500, err);
    console.log('cust err', err);
    return res.json(200, customers);
  });
};

exports.search = (req, res) => {
  Customer.find({name: {$regex: req.query.name, $options: "i"}}).exec(function(err, customers){
    if(err) return res.send(500, err);
    return res.json(200, customers);
  });
};

exports.show = (req, res) => {
  Customer.findById(req.params.id, function (err, customer) {
    if (err) return res.send(500, err);
    if (!customer) return res.send(404, { message: "Customer Not Found!" });
    return res.json(200, customer);
  });
};

exports.excel = (req, res) => {
  Customer.find().sort({ code: 1 }).exec(function(err, customers){
    if(err) return res.send(500, err);
    var lists = [];
    for(var i = 0; i < customers.length;i++) {
      lists.push({
        code: customers[i].code,
        name: customers[i].name,
        address: customers[i].address,
        phone: customers[i].phone,
        penjualan: customers[i].transaction.penjualan,
        hutang: customers[i].transaction.hutang
      });
    }

    res.json(200, lists);
  });
};

exports.backupTransaction = (req, res) => {
  Customer.find().exec(function(err, customers){
    if(err) return res.send(500, err);

    var lists = [];
    for(var i = 0; i < customers.length;i++) {
      lists.push({
        code: customers[i].code,
        name: customers[i].name,
        phone: customers[i].phone,
        penjualan: customers[i].transaction.penjualan,
        hutang: customers[i].transaction.hutang
      });
    }

    Q.all([resetTransaction()])
      .spread(function(results){
        console.log(results);
        res.json(200, lists);
      }).then(null, function(err){
        if(err) return res.send(500, err);
      });
  });
};

function resetTransaction() {
  return Customer.update({}, { $set: { transaction: { penjualan: 0, hutang: 0 } } }, { multi: true }).then(function(result){
    return result;
  }).then(null, function(err){
    if(err) return err;
  });
}

exports.create = (req, res) => {
  var body = req.body;
  body.code = "CS";
  Customer.create(body, function (err, customer) {
    if (err) return handleError(res, err);
    return res.json(201, customer);
  });
};

exports.update = (req, res) => {
  if (req.body._id) { delete req.body._id };
  Customer.findById(req.params.id, function (err, customer) {
    if (err) return handleError(res, err);
    if (!customer) return res.send(404, { message: 'Customer Not Found!' });
    let updated = _.merge(customer, req.body);
    updated.save(function (err) {
      if (err) return handleError(res, err);
      return res.json(200, customer);
    });
  });
};

exports.destroy = (req, res) => {
  Customer.findById(req.params.id, function (err, customer) {
    if (err) return handleError(res, err);
    if (!customer) return res.send(404, { message: 'Customer not Found' });
    customer.remove(function (err) {
      if (err) return handleError(res, err);
      return res.send(204);
    })
  });
};

function handleError(res, err) {
  return res.send(500, err);
}