const _ = require('lodash');
const Penjualan = require('./penjualan.model');
const Customer = require('../customer/customer.model');
const Barang = require('../barang/barang.model');
const Stock = require('../stock/stock.model');
const Q = require('q');

exports.index = (req, res) => {
  console.log(req.query);
  Penjualan.paginate({ code: { $regex: req.query.code, $options: "i" }, customerName: {$regex: req.query.customerName, $options: "i"} }, { sort: {created: -1 }, populate: 'customer detail.barang', page: Number(req.query.page), limit: Number(req.query.limit) }, function (err, orders) {
    if (err) return res.send(500, err);
    return res.json(200, orders);
  });
};

exports.show = (req, res) => {
  Penjualan.findOne({ _id: req.params.id }).populate('customer detail.barang').exec(function (err, order) {
    if (err) return res.send(500, err);
    if (!order) return res.send(404, { message: "Penjualan Not Found!" });
    return res.json(200, order);
  });
};

exports.report = (req, res) => {
  console.log('req query', req.query);
  var tgl = new Date();
  var day = tgl.getDate();
  var month = tgl.getMonth();
  var year = tgl.getFullYear();
  if(!req.query.reportFilter) {
    var ad = new Date(year, month, day, 00, 00, 01);
    var sd = new Date(year, month, day, 23, 59, 59);
  } else if(req.query.reportFilter === 'hari') {
    var ad = new Date(year, month, day, 00, 00, 01);
    var sd = new Date(year, month, day, 23, 59, 59);
  } else {
    var ad = new Date(year, 0, 1);
    var sd = new Date(year, 11, 31, 23, 59,59);
  }
  
  Penjualan.paginate({customerName: {$regex: req.query.customerName, $options: 'i'}, created: {$gte: ad, $lte: sd}}, {populate: 'customer detail.barang', page: Number(req.query.page), limit: Number(req.query.limit)}, function(err, penjualan){
    if(err) return res.send(500, err);
    return res.json(200, penjualan);
  });
};

exports.create = (req, res) => {
  var body = req.body;
  var sameData = [];
  var promises = [];
  var total = 0;
  console.log('body', body);
  body.detail = body.items;
  // return res.send(500);
  body.code = "PJ";
  body.detail.map(function(dt){
    dt.barang = dt._id;
    // sameData = body.extras.findIndex(function(extr){
    //   return extr.barang.toString() === dt.barang.toString();
    // });
    // if(sameData > -1 ) {
    //   dt.qty += body.extras[sameData].qty;
    //   body.extras.splice(sameData, 1);
    // }
    // dt.qty = checkSatuan(dt.qty, dt.satuan);
    dt.subtotal = dt.qty * dt.price;
    total += dt.subtotal;
    delete dt.total;
  });
  delete body.items;
  // body.detail = body.detail.concat(body.extras);
  // delete body.extras;
  body.grandTotal = total;
  body.customerName = body.customer ? body.customer.name : 'Tidak Terdaftar';
  console.log('body', body);
  // return res.json(200);
  body.customer = body.customer ? body.customer._id : null;
  Penjualan.create(body, function(err, order){
    if(err) return res.send(500, err);
    body.detail.map(function(dt){
      promises.push(changeStock(dt));
    });
    Q.all(promises).then(function(data){
      console.log(data);
      if(body.customer) {
        Customer.findOne({_id: body.customer}).exec(function(err, result){
          if(err) return res.send(500, err);
          if(!result) return res.send(404, {message: 'Customer Tidak Terdaftar!'});
          result.transaction.penjualan += body.grandTotal;
          if(body.uang) {
            result.transaction.hutang = body.uang < body.grandTotal ? result.transaction.hutang + (body.grandTotal - body.uang) : result.transaction.hutang;
            if(body.uang < body.grandTotal) {
              result.hutangLogs.push({
                total: (body.grandTotal - body.uang)
              });
            }
          }
          result.markModified('hutangLogs');
          result.save(function(err){
            if(err) return res.send(500, err);
            return res.json(201, order);
          });
        });
      } else {
        return res.json(201, order);
      }
    }).then(null, function(err){
      if(err) return res.send(500, err);
    });
  });
};

function changeStock(barang) {
  return Barang.findOne({_id: barang.barang}).exec().then(function(brg){
    if(!brg) return {status: 'not_exist', barang: brg};
    return Stock.findOne({_id: brg.stock}).exec().then(function(stok){
      if(!stok) return {status: "not_exist"};
      stok.stock -= barang.qty;
      // stok.primaryPrice = barang.price;
      stok.out += barang.qty;
      return stok.save().then(function(data){
        return {status: "updated_stok", barang: barang};
      });
    }).then(null, function(err){
      if(err) console.log(err);
    });
  }).then(null, function(err){
    if(err) console.log(err);
  });
}

function checkSatuan(qty, satuan) {
  var newQty = 0;
  switch(satuan) {
    case "lsn":
    newQty = qty * 12;
    break;

    case "kodi":
    newQty = qty * 20;
    break;

    case "gross":
    newQty = qty * 144;
    break;

    default:
    newQty = qty;
  }
  return newQty;
}


exports.update = (req, res) => {
  if (req.body._id) { delete req.body._id };
  Penjualan.findById(req.params.id, function (err, penjualan) {
    if (err) return handleError(res, err);
    if (!penjualan) return res.send(404, { message: 'Penjualan Not Found!' });
    let updated = _.merge(penjualan, req.body);
    updated.save(function (err) {
      if (err) return handleError(res, err);
      return res.json(200, penjualan);
    });
  });
};

exports.destroy = (req, res) => {
  Penjualan.findById(req.params.id, function (err, penjualan) {
    if (err) return handleError(res, err);
    if (!penjualan) return res.send(404, { message: 'Penjualan not Found' });
    penjualan.remove(function (err) {
      if (err) return handleError(res, err);
      return res.send(204);
    })
  });
};

function handleError(res, err) {
  return res.send(500, err);
}