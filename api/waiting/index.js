const express = require('express');
const router = express.Router();
const controller = require('./waiting.controller');

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.post('/bayar/:id', controller.bayar);
router.put('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;